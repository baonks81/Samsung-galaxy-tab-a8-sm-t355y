#!/system/bin/sh
#
# ZRAM activation
#
# Copyright (C) 2012 The CyanogenMod Project
#      Author: Humberto Borba <humberos@gmail.com>
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

### Zram Reset
echo 0 > /sys/block/zram0/disksize
echo "[ZRAM]: DISABLING";
swapoff /dev/block/zram0
echo 1 > /sys/block/zram0/reset
setprop vendor.zram.enable 0
echo "[ZRAM]: OFF";
###

ZRAM="persist.service.zram"

if [ "$(getprop $ZRAM)" = "0" ]; then

    echo "[ZRAM]: NOT ACTIVATED.";
    exit 1

else

    echo "[ZRAM]: INIT";

    if ! test -e /sys/block/zram0/disksize ; then

        echo "[ZRAM]: ERROR unable to find /sys/block/zram0/disksize";
        echo "[ZRAM]: NOT ACTIVATED IN KERNEL.";
        exit 1

    else

        ZRAM_VALUE="$(getprop $ZRAM)"
        echo "[ZRAM]: ZRAM_VALUE $ZRAM_VALUE";

       
       
        DISKSIZE_VALUE="$(getprop $DISKSIZE)"
        MEMTOTAL="$(grep MemTotal /proc/meminfo | awk ' { print $2 } ')"
        echo "[ZRAM]: MEMTOTAL $MEMTOTAL";
        let "DISKSIZE_VALUE=MEMTOTAL*ZRAM_VALUE/100*1024"
       
        echo "[ZRAM]: DISKSIZE $DISKSIZE_VALUE";

        if (( "$DISKSIZE_VALUE" > 0 )) ; then

            echo "[ZRAM]: Setting ZRAM disksize.";
            echo $DISKSIZE_VALUE > /sys/block/zram0/disksize

            echo "[ZRAM]: Starting ZRAM.";
            mkswap /dev/block/zram0
            swapon -p -1 /dev/block/zram0
            echo "[ZRAM]: ACTIVATED.";

        else

            echo "[ZRAM]: Invalid Disk Size.";
            exit 1

        fi

    fi

fi

exit 0
