#!/system/bin/sh
# Written by Draco (tytydraco @ GitHub)

# The name of the current branch for logging purposes
BRANCH="latency"

# Maximum unsigned integer size in C
UNIT_MAX="998400"

# Minimum frequencies
UNIT_MIN="200000"

# Duration in nanoseconds of one scheduling period
SCHED_PERIOD="10000000" # "$((10 * 1000 * 1000))"

# How many tasks should we have at a maximum in one scheduling period
SCHED_TASKS="10"

write() {
	# Bail out if file does not exist
	[[ ! -f "$1" ]] && return 1

	# Make file writable in case it is not already
	chmod +w "$1" 2> /dev/null

	# Write the new value and bail if there's an error
	if ! echo "$2" > "$1" 2> /dev/null
	then
		echo "Failed: $1 → $2"
		return 1
	fi

	# Log the success
	echo "$1 → $2"
}

# Check for root permissions and bail if not granted
if [[ "$(id -u)" -ne 0 ]]
then
	echo "No root permissions. Exiting."
	exit 1
fi

# Detect if we are running on Android
grep -q android /proc/cmdline && ANDROID=true

# Log the date and time for records sake
echo "Time of execution: $(date)"
echo "Branch: ${BRANCH}"

# Sync to data in the rare case a device crashes
sync

# Limit max perf event processing time to this much CPU usage
write /proc/sys/kernel/perf_cpu_time_max_percent 3

# Group tasks for less stutter but less throughput
write /proc/sys/kernel/sched_autogroup_enabled 1

# Execute child process before parent after fork
write /proc/sys/kernel/sched_child_runs_first 0

# Preliminary requirement for the following values
write /proc/sys/kernel/sched_tunable_scaling 0

# Reduce the maximum scheduling period for lower latency
write /proc/sys/kernel/sched_latency_ns 10000000

# Schedule this ratio of tasks in the guarenteed sched period
write /proc/sys/kernel/sched_min_granularity_ns 2250000

# Require preeptive tasks to surpass half of a sched period in vmruntime
write /proc/sys/kernel/sched_wakeup_granularity_ns 2000000

# Reduce the frequency of task migrations
write /proc/sys/kernel/sched_migration_cost 500000

# Always allow sched boosting on top-app tasks
[[ "$ANDROID" == true ]] && write /proc/sys/kernel/sched_min_task_util_for_colocation 0

# Improve real time latencies by reducing the scheduler migration time
write /proc/sys/kernel/sched_nr_migrate 32

# Disable scheduler statistics to reduce overhead
write /proc/sys/kernel/sched_schedstats 0

# Disable unnecessary printk logging
write /proc/sys/kernel/printk_devkmsg off

# Start non-blocking writeback later
write /proc/sys/vm/dirty_background_ratio 5

# Start blocking writeback later
write /proc/sys/vm/dirty_ratio 15

# Require dirty memory to stay in memory for longer
write /proc/sys/vm/dirty_expire_centisecs 200

# Run the dirty memory flusher threads less often
write /proc/sys/vm/dirty_writeback_centisecs 400

# Disable read-ahead for swap devices
write /proc/sys/vm/page-cluster 3

# Update /proc/stat less often to reduce jitter
write /proc/sys/vm/stat_interval 1

# Swap to the swap device at a fair rate
write /proc/sys/vm/swappiness 100

# Prioritize page cache over simple file structure nodes
write /proc/sys/vm/vfs_cache_pressure 100

# Allow LMK to free more RAM
write /proc/sys/vm/highmem_is_dirtyable 0

# Not usually clear cache
write /proc/sys/vm/drop_caches 3

# Enable Explicit Congestion Control
write /proc/sys/net/ipv4/tcp_ecn 1

# Enable fast socket open for receiver and sender
write /proc/sys/net/ipv4/tcp_fastopen 3

# Disable SYN cookies
write /proc/sys/net/ipv4/tcp_syncookies 0

# Disable sysrq from keyboard, Marshmallow does this
write /proc/sys/kernel/sysrq 0

# ram tuning
# these are recommentations for 2gb devices
setprop dalvik.vm.heapstartsize 8m
setprop dalvik.vm.heapgrowthlimit 128m
setprop dalvik.vm.heapsize 512m
setprop dalvik.vm.heaptargetutilization 0.75
setprop dalvik.vm.heapminfree 2m
setprop dalvik.vm.heapmaxfree 8m

for f in /sys/fs/ext4/*; 
do 
  test "$f" = "/sys/fs/ext4/features" && continue 
  write "${f}/max_writeback_mb_bump" 128 # don't spend too long writing ONE file if multiple need to write 
done

if test -e /sys/block/dm-0; then
  for f in /sys/block/dm-*; do # encrypted filesystems
    write "${f}/queue/rq_affinity" 2 # moving cpus is "expensive"
    write "${f}/queue/rotational" 0
  done
fi

if [[ -f "/sys/kernel/debug/sched_features" ]]
then
	# Consider scheduling tasks that are eager to run
	write /sys/kernel/debug/sched_features NEXT_BUDDY

	# Some sources report large latency spikes during large migrations
	write /sys/kernel/debug/sched_features NO_TTWU_QUEUE
fi

[[ "$ANDROID" == true ]] && if [[ -d "/dev/stune/" ]]
then
	# Prefer to schedule top-app tasks on idle CPUs
	write /dev/stune/top-app/schedtune.prefer_idle 1

	# Mark top-app as boosted, find high-performing CPUs
	write /dev/stune/top-app/schedtune.boost 1
fi

# Loop over each CPU in the system
for cpu in /sys/devices/system/cpu/cpu*/cpufreq
do
	# Fetch the available governors from the CPU
	avail_govs="$(cat "${cpu}/scaling_available_governors")"

	# Attempt to set the governor in this order
	for governor in interactive intelliactive ondemand conservative
	do
		# Once a matching governor is found, set it and break for this CPU
		if [[ "${avail_govs}" == *"${governor}"* ]]
		then
			write "${cpu}/scaling_governor" "${governor}"
			break
		fi
	done
done

# Apply governor specific tunables for intelliactive
find /sys/devices/system/cpu/ -name intelliactive -type d | while IFS= read -r governor
do
	# Consider changing frequencies once per scheduling period
	write "${governor}/above_hispeed_delay" "25000 1000000:50000"
  write "${governor}/boost" 0
  write "${governor}/boostpulse_duration" 80000
  write "${governor}/io_is_busy" 0
  write "${governor}/target_loads" "1 760000:85 860000:90 1000000:80"
  write "${governor}/timer_rate" 50000
  write "${governor}/timer_slack" 80000
	write "${governor}/min_sample_time" 50000
  write "${governor}/sampling_down_factor" 50000
  write "${governor}/sync_freq" 513000

	# Jump to hispeed frequency at this load percentage
	write "${governor}/go_hispeed_load" 90
	write "${governor}/hispeed_freq" 860000
  write "${governor}/two_phase_freq" "513000,640000,760000,860000"
  write "${governor}/up_threshold_any_cpu_freq" 860000
  write "${governor}/up_threshold_any_cpu_load" 90
done

# Apply governor specific tunables for interactive
find /sys/devices/system/cpu/ -name interactive -type d | while IFS= read -r governor
do
  # CPU frequencies 200000 400000 533333 800000 998400 1094400 1190400
	# Consider changing frequencies once per scheduling period
	write "${governor}/above_hispeed_delay" "20000 600000:40000 1000000:20000" # 20000 # "25000 1094400:50000"
  write "${governor}/boost" 0
  write "${governor}/boostpulse_duration" 0 # 80000
  write "${governor}/io_is_busy" 1
  write "${governor}/max_freq_hysteresis" 0
	write "${governor}/min_sample_time" 40000 # 50000
  write "${governor}/target_loads" "85 800000:90 1100000:70" # 90 # "1 800000:85 998400:90 1094400:80"
  write "${governor}/timer_rate" 20000 #50000
  write "${governor}/timer_slack" -1 # 80000
  write "${governor}/user_migration_notif" 0
  write "${governor}/use_sched_load" 0

	# Jump to hispeed frequency at this load percentage
	write "${governor}/go_hispeed_load" 90
	write "${governor}/hispeed_freq" 998400

	chmod 0777 /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq
	write /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq 1094400

	chmod 0777 /sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq
	write /sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq 800000

  write /sys/module/msm_thermal/core_control/enabled 1
  write /sys/module/msm_thermal/parameters/enabled Y

  write /sys/module/msm_performance/parameters/max_cpus 4

done

# Apply governor specific tunables for conservative
find /sys/devices/system/cpu/ -name conservative -type d | while IFS= read -r governor
do
	# Consider changing frequencies once per scheduling period
	write "${governor}/down_threshold" 20
	write "${governor}/freq_step" 5
	write "${governor}/ignore_nice_load" 0
	write "${governor}/sampling_down_factor" 1
	write "${governor}/sampling_rate" 300000
	write "${governor}/sampling_rate_min" 200000

	# Jump to hispeed frequency at this load percentage
	write "${governor}/up_threshold" 80
done

# Apply governor specific tunables for ondemand
find /sys/devices/system/cpu/ -name ondemand -type d | while IFS= read -r governor
do
	# Consider changing frequencies once per scheduling period
  write "${governor}/ignore_nice_load" 0
	write "${governor}/io_is_busy" 0
	write "${governor}/powersave_bias" 0
	write "${governor}/sampling_down_factor" 1
	write "${governor}/sampling_rate" 20000 #300000

	# Jump to hispeed frequency at this load percentage
	write "${governor}/up_threshold" 95

done

for queue in /sys/block/loop0/queue
do
	# Choose the first governor available
	avail_scheds="$(cat "${queue}/scheduler")"
	for sched in noop deadline row [cfq] none kyber bfq mq-deadline
	do
		if [[ "${avail_scheds}" == *"${sched}"* ]]
		then
			write "${queue}/scheduler" "${sched}"
			break
		fi
	done

	# Do not use I/O as a source of randomness
	write "${queue}/add_random" 0

	# Disable I/O statistics accounting
	write "${queue}/iostats" 0

	# Reduce the maximum number of I/O requests in exchange for latency
  chmod 0777 "${queue}"/nr_requests
	write "${queue}/nr_requests" 128

	# Reduce heuristic read-ahead in exchange for I/O latency
	write "${queue}/read_ahead_kb" 128

	write "${queue}/rotational" 1

	# Move cpus is "expensive"
	write "${queue}/rq_affinity" 0

done

for loop in $(ls /sys/block/ | grep -E '(loop[1-7])')
do
	# Choose the first governor available
  queue="/sys/block/${loop}/queue"
	avail_scheds="$(cat "${queue}/scheduler")"
	for sched in noop deadline row [cfq] none kyber bfq mq-deadline
	do
		if [[ "${avail_scheds}" == *"${sched}"* ]]
		then
			write "${queue}/scheduler" "${sched}"
			break
		fi
	done

	# Do not use I/O as a source of randomness
	write "${queue}/add_random" 0

	# Disable I/O statistics accounting
	write "${queue}/iostats" 0

	# Reduce the maximum number of I/O requests in exchange for latency
  chmod 0777 "${queue}"/nr_requests
	write "${queue}/nr_requests" 128

	# Reduce heuristic read-ahead in exchange for I/O latency
	write "${queue}/read_ahead_kb" 128

	write "${queue}/rotational" 1

	# Move cpus is "expensive"
	write "${queue}/rq_affinity" 0

done

for queue in /sys/block/mmcblk0/queue
do
	# Choose the first governor available
	avail_scheds="$(cat "${queue}/scheduler")"
	for sched in noop deadline row [cfq] none kyber bfq mq-deadline
	do
		if [[ "${avail_scheds}" == *"${sched}"* ]]
		then
			write "${queue}/scheduler" "${sched}"
			break
		fi
	done

	# Do not use I/O as a source of randomness
	write "${queue}/add_random" 1

	# Disable I/O statistics accounting
	write "${queue}/iostats" 1

	# Reduce the maximum number of I/O requests in exchange for latency
  chmod 0777 "${queue}"/nr_requests
	write "${queue}/nr_requests" 128

	# Reduce heuristic read-ahead in exchange for I/O latency
	write "${queue}/read_ahead_kb" 128

	write "${queue}/rotational" 0

	# Move cpus is "expensive"
	write "${queue}/rq_affinity" 1

  write "${queue}"/iosched/back_seek_max 16384 # default 2147483647 i.e. the whole disk
  write "${queue}"/iosched/back_seek_penalty 2 # no penalty
  write "${queue}"/iosched/fifo_expire_async 250
  write "${queue}"/iosched/fifo_expire_sync 120
  write "${queue}"/iosched/group_idle 0 # BUT make sure there is differentilation between cgroups
  write "${queue}"/iosched/low_latency 1
  write "${queue}"/iosched/quantum 8 # default 8. Removes bottleneck
  write "${queue}"/iosched/slice_async 40
  write "${queue}"/iosched/slice_async_rq 2 # default 2. See above
  write "${queue}"/iosched/slice_idle 0 # never idle WITHIN groups
  write "${queue}"/iosched/slice_sync 100
  write "${queue}"/iosched/target_latency 300

done

for queue in /sys/block/mmcblk1/queue
do
	# Choose the first governor available
	avail_scheds="$(cat "${queue}/scheduler")"
	for sched in noop deadline row [cfq] none kyber bfq mq-deadline
	do
		if [[ "${avail_scheds}" == *"${sched}"* ]]
		then
			write "${queue}/scheduler" "${sched}"
			break
		fi
	done

	# Do not use I/O as a source of randomness
	write "${queue}/add_random" 1

	# Disable I/O statistics accounting
	write "${queue}/iostats" 1

	# Reduce the maximum number of I/O requests in exchange for latency
  chmod 0777 "${queue}"/nr_requests
	write "${queue}/nr_requests" 128

	# Reduce heuristic read-ahead in exchange for I/O latency
	write "${queue}/read_ahead_kb" 128

	write "${queue}/rotational" 0

	# Move cpus is "expensive"
	write "${queue}/rq_affinity" 1

  write "${queue}"/iosched/back_seek_max 16384 # default 2147483647 i.e. the whole disk
  write "${queue}"/iosched/back_seek_penalty 2 # no penalty
  write "${queue}"/iosched/fifo_expire_async 250
  write "${queue}"/iosched/fifo_expire_sync 120
  write "${queue}"/iosched/group_idle 0 # BUT make sure there is differentilation between cgroups
  write "${queue}"/iosched/low_latency 1
  write "${queue}"/iosched/quantum 8 # default 8. Removes bottleneck
  write "${queue}"/iosched/slice_async 40
  write "${queue}"/iosched/slice_async_rq 2 # default 2. See above
  write "${queue}"/iosched/slice_idle 0 # never idle WITHIN groups
  write "${queue}"/iosched/slice_sync 100
  write "${queue}"/iosched/target_latency 300

done


for queue in /sys/block/mmcblk0/mmcblk0rpmb/queue
do
	# Choose the first governor available
	avail_scheds="$(cat "${queue}/scheduler")"
	for sched in noop deadline row [cfq] none kyber bfq mq-deadline
	do
		if [[ "${avail_scheds}" == *"${sched}"* ]]
		then
			write "${queue}/scheduler" "${sched}"
			break
		fi
	done

	# Do not use I/O as a source of randomness
	write "${queue}/add_random" 1

	# Disable I/O statistics accounting
	write "${queue}/iostats" 1

	# Reduce the maximum number of I/O requests in exchange for latency
  chmod 0777 "${queue}"/nr_requests
	write "${queue}/nr_requests" 128

	# Reduce heuristic read-ahead in exchange for I/O latency
	write "${queue}/read_ahead_kb" 128

	write "${queue}/rotational" 0

	# Move cpus is "expensive"
	write "${queue}/rq_affinity" 1

  write "${queue}"/iosched/back_seek_max 16384 # default 2147483647 i.e. the whole disk
  write "${queue}"/iosched/back_seek_penalty 2 # no penalty
  write "${queue}"/iosched/fifo_expire_async 250
  write "${queue}"/iosched/fifo_expire_sync 120
  write "${queue}"/iosched/group_idle 0 # BUT make sure there is differentilation between cgroups
  write "${queue}"/iosched/low_latency 1
  write "${queue}"/iosched/quantum 8 # default 8. Removes bottleneck
  write "${queue}"/iosched/slice_async 40
  write "${queue}"/iosched/slice_async_rq 2 # default 2. See above
  write "${queue}"/iosched/slice_idle 0 # never idle WITHIN groups
  write "${queue}"/iosched/slice_sync 100
  write "${queue}"/iosched/target_latency 300

done

for queue in /sys/block/ram*/queue
do
	# Choose the first governor available
	avail_scheds="$(cat "${queue}/scheduler")"
	for sched in noop deadline row [cfq] none kyber bfq mq-deadline
	do
		if [[ "${avail_scheds}" == *"${sched}"* ]]
		then
			write "${queue}/scheduler" "${sched}"
			break
		fi
	done

	# Do not use I/O as a source of randomness
	write "${queue}/add_random" 0

	# Disable I/O statistics accounting
	write "${queue}/iostats" 0

	# Reduce the maximum number of I/O requests in exchange for latency
  chmod 0777 "${queue}"/nr_requests
	write "${queue}/nr_requests" 128

	# Reduce heuristic read-ahead in exchange for I/O latency
	write "${queue}/read_ahead_kb" 128

	write "${queue}/rotational" 1

	# Move cpus is "expensive"
	write "${queue}/rq_affinity" 0

done

for queue in /sys/block/vnswap0/queue
do
	# Choose the first governor available
	avail_scheds="$(cat "${queue}/scheduler")"
	for sched in noop deadline row [cfq] none kyber bfq mq-deadline
	do
		if [[ "${avail_scheds}" == *"${sched}"* ]]
		then
			write "${queue}/scheduler" "${sched}"
			break
		fi
	done

	# Do not use I/O as a source of randomness
	write "${queue}/add_random" 0

	# Disable I/O statistics accounting
	write "${queue}/iostats" 0

	# Reduce the maximum number of I/O requests in exchange for latency
  chmod 0777 "${queue}"/nr_requests
	write "${queue}/nr_requests" 128

	# Reduce heuristic read-ahead in exchange for I/O latency
	write "${queue}/read_ahead_kb" 128

	write "${queue}/rotational" 0

	# Move cpus is "expensive"
	write "${queue}/rq_affinity" 0

done

for queue in /sys/block/zram0/queue
do
	# Choose the first governor available
	avail_scheds="$(cat "${queue}/scheduler")"
	for sched in noop deadline row [cfq] none kyber bfq mq-deadline
	do
		if [[ "${avail_scheds}" == *"${sched}"* ]]
		then
			write "${queue}/scheduler" "${sched}"
			break
		fi
	done

	# Do not use I/O as a source of randomness
	write "${queue}/add_random" 0

	# Disable I/O statistics accounting
	write "${queue}/iostats" 0

	# Reduce the maximum number of I/O requests in exchange for latency
  chmod 0777 "${queue}"/nr_requests
	write "${queue}/nr_requests" 128

	# Reduce heuristic read-ahead in exchange for I/O latency
	write "${queue}/read_ahead_kb" 128

	write "${queue}/rotational" 1

	# Move cpus is "expensive"
	write "${queue}/rq_affinity" 0

done

chmod 0777 /sys/module/lowmemorykiller/parameters
write /sys/module/lowmemorykiller/parameters/adj "0,100,200,300,900,906" # default "0,58,117,235,411,1000" https://android.googlesource.com/platform/frameworks/base/+/master/services/core/java/com/android/server/am/ProcessList.java#50
write /sys/module/lowmemorykiller/parameters/adj_max_shift 606
write /sys/module/lowmemorykiller/parameters/cost 32 # default 48
write /sys/module/lowmemorykiller/parameters/debug_level 1
write /sys/module/lowmemorykiller/parameters/enable_adaptive_lmk 0
write /sys/module/lowmemorykiller/parameters/lmkcount 0
write /sys/module/lowmemorykiller/parameters/minfree "8192,10240,12288,14336,16384,20480" # "12288,15360,18432,21504,24576,30720" # "18432,23040,27648,32256,36864,46080" - "8192,10240,12288,14336,16384,20480" # the same as Moto G 5.1, and AOSP 4.x
write /sys/module/lowmemorykiller/parameters/vmpressure_file_min 53059
chmod 0444 /sys/module/lowmemorykiller/parameters # so android can't edit it

for queue in /sys/block/loop*/bdi
do

	write "${queue}/read_ahead_kb" 256

done

for queue in /sys/block/mmcblk0/bdi
do

	write "${queue}/read_ahead_kb" 512

done

for queue in /sys/block/mmcblk0rpmb/bdi
do

	write "${queue}/read_ahead_kb" 256

done

for queue in /sys/block/mmcblk1/bdi
do

	write "${queue}/read_ahead_kb" 256

done

for queue in /sys/block/ram*/bdi
do

	write "${queue}/read_ahead_kb" 256

done

for queue in /sys/block/vnswap0/bdi
do

	write "${queue}/read_ahead_kb" 256

done

for queue in /sys/block/zram0/bdi
do

	write "${queue}/read_ahead_kb" 256

done

# Always return success, even if the last write fails
exit 0
